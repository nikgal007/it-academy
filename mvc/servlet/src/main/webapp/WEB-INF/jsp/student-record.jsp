<%@ page import ="by.galov.Student" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>Student Record</title>
    </head>
    <body>
    <h1>list</h1>
        <c:set var="alphabet" value="${['a','b']}" scope="application" />
        <c:out value="${alphabet}"/>
    <h1>list items</h1>
        <c:forEach items="${alphabet}" var="item">
            <div>
                <c:out value="${item}"/>
            </div>
        </c:forEach>
     <h1>list items</h1>
            <c:forEach items="${students}" var="student">
                <div>
                    <c:out value="${student}"/>
                </div>
            </c:forEach>
    <%! private int counter = 0; %>
    <%
        if (request.getAttribute("studentRecord") != null) {
            Student student = (Student) request.getAttribute("studentRecord");
    %>
    <h1>Student Record</h1>
    <%= ++counter %>
    <div>ID: <%= student.getId()%></div>
    <div>First Name: <%= student.getFirstName()%></div>
    <div>Last Name: <%= student.getLastName()%></div>

    <%
        } else {
    %>
    <%@include file="error.jsp" %>

    <% } %>
    </body>
</html>