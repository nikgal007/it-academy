package by.galov;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class StudentService {

    public Optional<Student> getStudent(int id) {
        switch (id) {
            case 1:
                return Optional.of(new Student(1, "John", "Doe"));
            case 2:
                return Optional.of(new Student(2, "Jane", "Goodall"));
            case 3:
                return Optional.of(new Student(3, "Max", "Born"));
            default:
                return Optional.empty();
        }
    }

    public List<Student> getStudents() {
        return Arrays.asList(
                new Student(1, "John", "Doe"),
                new Student(2, "John", "Doe"),
                new Student(3, "John", "Doe")
        );
    }
}