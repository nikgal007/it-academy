package com.example.spring.repos;

import com.example.spring.dto.Employee;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Pageable;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class EmployeeRepositoryTest {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Test
    void testRep() {
        Employee employee = new Employee();
        employee.setName("Nick");
        Employee employee1 = new Employee();
        employee1.setName("Vasya");
        employeeRepository.saveAndFlush(employee);
        employeeRepository.saveAndFlush(employee1);
        System.out.println(employeeRepository.findByName("Nick"));
        System.out.println("page : " + employeeRepository.findAll(Pageable.ofSize(1)));
        assertTrue(employeeRepository.existsById(employee.getId()));
        assertTrue(employeeRepository.exists(Example.of(employee)));
        assertFalse(employeeRepository.existsById(employee.getId() + 1234));
    }

}