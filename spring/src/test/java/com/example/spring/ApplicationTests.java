package com.example.spring;

import com.example.spring.academy.HelloController;
import com.example.spring.academy.HelloServiceInterface;
import com.example.spring.aspect.UsageTracked;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest
class ApplicationTests {
	@Autowired
	private WebApplicationContext webApplicationContext;

	@Test
	void contextLoads() {
		HelloServiceInterface helloServiceInterface = webApplicationContext.getBean(HelloServiceInterface.class);
		UsageTracked usageTracked = (UsageTracked)webApplicationContext.getBean(HelloServiceInterface.class);
		usageTracked.trackUsage();
		helloServiceInterface.getHello();
	}

}
