package com.example.spring;

import com.example.spring.academy.HelloController;
import com.example.spring.academy.TestBean;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringTest {

    @Test
    public void contextTest() {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("beans.xml");
        context.getBean("helloController", HelloController.class).sayHello();
    }

    @Test
    public void proto() {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("beans.xml");
        System.out.println(
                context.getBean("testBean", TestBean.class).getValue());
        System.out.println(
                context.getBean("testBean", TestBean.class).getValue());
    }

    @Test
    public void props() {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("beans.xml");
        System.out.println(
                context.getBean("testBean", TestBean.class).getProps().get("12"));

    }

    @Test
    public void spel() {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("beans.xml");
        System.out.println(
                context.getBean("testBean1", TestBean.class).getName());

    }


}
