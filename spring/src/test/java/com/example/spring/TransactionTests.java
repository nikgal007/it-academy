package com.example.spring;

import com.example.spring.academy.Counter;
import com.example.spring.academy.CounterB;
import com.example.spring.dto.Employee;
import com.example.spring.repos.EmployeeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

@SpringBootTest
//@Transactional(propagation = NOT_SUPPORTED)
public class TransactionTests {

    @Autowired
    private PlatformTransactionManager transactionManager;

    @Autowired
    private EmployeeRepository employeeRepository;

    private TransactionTemplate transactionTemplate;

    @Autowired
    private Counter counter;

    @Autowired
    private CounterB counterB;

    @BeforeEach
    public void setUp() {
        transactionTemplate = new TransactionTemplate(transactionManager);
    }

    @Test
    @Rollback(value = false)
    public void test() {
        Employee employee = new Employee();
        employee.setName("1234");
        employeeRepository.save(employee);
        System.out.println(employeeRepository.findAll());
    }

    @Test
    void givenTwoPayments_WhenRefIsDuplicate_ThenShouldRollback() {
        try {
            transactionTemplate.execute(status -> {
                Employee employee = new Employee();
                employee.setName("1234");
                employeeRepository.save(employee);

                fail();
                return "";
            });
        } catch (Exception ignored) {
        }

        System.out.println(employeeRepository.findAll());
    }

    private void fail() {
        throw new RuntimeException();
    }

    @Test
    void givenTwoPayments_WhenMarkAsRollback_ThenShouldRollback() {

        try {
            transactionTemplate.execute(status -> {
                Employee employee = new Employee();
                employee.setName("1234");
                employeeRepository.save(employee);

                status.setRollbackOnly();
                return "dfasdf";
            });
        } catch (Exception ignored) {
        }

        System.out.println(employeeRepository.findAll());
    }

    @Test
    void givenAPayment_WhenNotExpectingAnyResult_ThenShouldCommit() {
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                Employee employee = new Employee();
                employee.setName("1234");
                employeeRepository.save(employee);
                status.setRollbackOnly();
            }
        });

    }

//    @Test
//    public void tt1() {
//        counter.countAandB();
//        counter.print();
//        counterB.print();
//    }

    @Test
    public void tt2() {
        counterB.countB();
        counterB.print();
    }

}
