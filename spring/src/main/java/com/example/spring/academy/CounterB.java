package com.example.spring.academy;

import lombok.Data;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
@Data
public class CounterB {
    private Integer b = 0;

    @Transactional(propagation = Propagation.NEVER)
    public void countB() {
        b++;
    }

    public void fail() {
        throw new RuntimeException();
    }

    public void print() {
        System.out.println( " b : " + b);
    }

}
