package com.example.spring.academy;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Data
@Component
public class HelloController {
    @Autowired
    private HelloServiceInterface helloService;

    public void sayHello() {
        System.out.println(helloService.getHello());
    }
}
