package com.example.spring.academy;

public class Notifier {
    public void notifyBefore() {
        System.out.println("before");
    }

    public void notifyAfter() {
        System.out.println("after");
    }
}
