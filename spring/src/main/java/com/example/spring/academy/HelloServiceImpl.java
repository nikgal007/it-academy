package com.example.spring.academy;

import org.springframework.stereotype.Service;

@Service
public class HelloServiceImpl implements HelloServiceInterface {
    @Override
    public String getHello() {
        return "hello";
    }
}
