package com.example.spring.academy;

import lombok.Data;

import java.util.Properties;

@Data
public class TestBean {
    private Properties props;
    private Double value = Math.random();
    private String name;
}
