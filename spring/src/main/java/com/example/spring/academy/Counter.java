package com.example.spring.academy;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
@Data
public class Counter {
    private Integer a = 0;

    @Autowired
    private CounterB counterB;

    @Transactional(noRollbackFor = RuntimeException.class)
    public void countAandB() {
        a++;
        counterB.countB();
    }

    public void print() {
        System.out.println("a : " + a);
    }

}
