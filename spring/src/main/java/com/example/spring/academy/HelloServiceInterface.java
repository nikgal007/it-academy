package com.example.spring.academy;

public interface HelloServiceInterface {
    String getHello();
}
