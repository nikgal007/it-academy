package com.example.spring.aspect;

public class DefaultUsageTracked implements UsageTracked {
    public static Integer counter = 0;
    @Override
    public void trackUsage() {
        System.out.println("counter : " + ++counter);
    }
}
