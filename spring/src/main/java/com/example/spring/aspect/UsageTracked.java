package com.example.spring.aspect;

public interface UsageTracked {
    void trackUsage();
}
