package com.example.spring.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.DeclareParents;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class IntoductionAspect {

    @DeclareParents(value = "com.example.spring.academy.HelloServiceImpl", defaultImpl = DefaultUsageTracked.class)
    public static UsageTracked mixin;

    @Before("execution(* *.getHello()) && this(usageTracked)")
    public void recordUsage(UsageTracked usageTracked) {
        usageTracked.trackUsage();
    }

}
