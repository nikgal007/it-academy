package com.example.spring.aspect;

import com.example.spring.dto.Employee;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.runtime.internal.AroundClosure;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Aspect
@Slf4j
@Data
@Component
public class MyAspect {

    @Pointcut("execution(public * com.example.spring.controllers.EmployeeController.save(com.example.spring.dto.Employee)) && args(employee)")
    public void intercept(Employee employee) {
    }

    @Pointcut("execution(public * com.example.spring.controllers.EmployeeController.*(..))")
    public void log() {
    }

    @Before("intercept(employee)")
    public void before(Employee employee) {
        System.out.println("before " + employee);
        log.info("before");
    }

    @Around("execution(public * *.sayHello())")
    public void around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        try {
            System.out.println("before hello");
            proceedingJoinPoint.proceed();
            System.out.println("after hello");
        } finally {
            System.out.println("finally");
        }
    }



    @After("log()")
    public void after() {
        System.out.println("after");
        log.info("after");
    }
}
