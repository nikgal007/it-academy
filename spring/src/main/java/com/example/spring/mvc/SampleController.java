package com.example.spring.mvc;

import com.example.spring.dto.Employee;
import com.example.spring.repos.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@Controller
@SessionAttributes("counter")
public class SampleController {
    @Autowired
    private EmployeeRepository employeeRepository;

    @ModelAttribute("counter")
    public AtomicInteger getVisitor() {
        return new AtomicInteger(0);
    }

    @GetMapping("/sample")
    public String showForm(ModelMap modelMap,
                           @ModelAttribute("counter") AtomicInteger counter,
                           @CookieValue(value = "JSESSIONID", required = false) String cookie) {
        System.out.println("cookie : " + cookie);
        System.out.println("counter : " + counter.incrementAndGet());
        modelMap.clear();
        modelMap.put("employee", new Employee());
        modelMap.put("employees", employeeRepository.findAll());
        return "sample";
    }

    @PostMapping("/add")
    public String addEmployee(ModelMap modelMap, Employee employee, BindingResult bindingResult) {
        Optional.ofNullable(employee).ifPresent(employeeRepository::save);
        modelMap.put("employees", employeeRepository.findAll());
        return "redirect:sample";
    }

    @PostMapping("/delete")
//    @Secured("ADMIN")
    @RolesAllowed("ADMIN")
    public String deleteEmployee(@RequestParam Integer id) {
        employeeRepository.deleteById(id);
        return "redirect:sample";
    }

}
