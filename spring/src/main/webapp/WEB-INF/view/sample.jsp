<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
   <head></head>

   <body>
      <sec:authorize access="hasRole('ADMIN')">
        <div>hello admin</div>
      </sec:authorize>
      <h1>This is the body of the sample view</h1>
      <table>
          <!-- here should go some titles... -->
          <tr>
              <th>id</th>
              <th>name</th>
              <th>delete</th>
          </tr>
          <c:forEach items="${employees}" var="item">
          <tr>
              <td>
                  <c:out value="${item.id}" />
              </td>
              <td>
                  <c:out value="${item.name}" />
              </td>
              <td>
                  <form action="delete" method="post">
                      <input type="hidden" name="id" value="${item.id}" />
                      <input type="submit" value="Delete" />
                  </form>
              </td>
          </tr>
          </c:forEach>
      </table>
      <f:form id="employeeForm" name="add" action="add" modelAttribute="employee" method="post">
           <fieldSet>
                <label for="name">Employee name</label>
                <f:input id="name" type="text" value="" path="name"/><br/>
                <input id="employeeButton" type="submit" value="add"/>
          </fieldSet>
      </f:form>
      <form action="/logout" method="post">
                  <input type="submit" value="Sign Out"/>
      </form>
   </body>
</html>