package com.company.project;

import lombok.*;

import javax.persistence.*;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "employee")
//@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
//@DiscriminatorColumn(name = "PERSON_TYPE", discriminatorType = DiscriminatorType.CHAR)
//@DiscriminatorValue("P")
@ToString(callSuper = true)
public class Employee
        extends Person
{
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "employee_id", unique = true)
//    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "department_id")
    @EqualsAndHashCode.Exclude
    private Department department;

    @OneToOne(cascade = CascadeType.ALL)
    @Access(AccessType.PROPERTY)
    private EmployeeDetails employeeDetails;

    public Employee(String name, String lastName) {
        super.setFirstName(name);
        super.setLastName(lastName);
    }
}
