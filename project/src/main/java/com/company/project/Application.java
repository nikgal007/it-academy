package com.company.project;

import org.hibernate.Session;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.sql.SQLException;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

//import org.h2.tools.Server;

public class Application {

    public static void main(String[] args) throws SQLException {
        Logger log = Logger.getLogger("org.hibernate.SQL");
        log.setLevel(Level.ALL);
        startDatabase();

        final EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("br.com.fredericci.pu");
        final EntityManager entityManager = entityManagerFactory.createEntityManager();

        Department department = new Department("depName");
        Employee employee = new Employee();
        employee.setFirstName("Dennys");
        employee.setLastName("Fredericci");
        employee.setDepartment(department);
        department.getEmployees().add(employee);
        EmployeeDetails employeeDetails = new EmployeeDetails(null, "detail", null);
        EmployeeDetails employeeDetails1 = new EmployeeDetails(null, "detail1", null);

        Session session = entityManager.unwrap(Session.class);


        session.beginTransaction();
        employeeDetails.setEmployee(employee);
        employee.setEmployeeDetails(employeeDetails);

        session.save(employee);
        Employee employee1 = new Employee( department, employeeDetails1 );
        employeeDetails1.setEmployee(employee1);
        employee1.setFirstName("nick");
        employee1.setLastName("galov");
        department.getEmployees().add(employee1);
        session.save(employee1);
        session.save(department);
        session.getTransaction().commit();

        System.out.println("Open your browser and navigate to http://localhost:8082/");
        System.out.println("JDBC URL: jdbc:h2:mem:my_database");
        System.out.println("User Name: sa");
        System.out.println("Password: ");
        System.out.println("Customer: " + employee);
        System.out.println("Person: " + entityManager.find(Person.class, employee.getId()));
        System.out.println("Employee: " + entityManager.find(Employee.class, employee.getId()));
        System.out.println("EmployeeDetails: " + entityManager.find(EmployeeDetails.class, employee.getEmployeeDetails().getId()));
        System.out.println("Department: " + entityManager.find(Department.class, employee.getDepartment().getId()));
        session.beginTransaction();
        Department dep = session.find(Department.class, employee.getDepartment().getId());
        Set<Employee> employees = dep.getEmployees();
        session.getTransaction().commit();
        System.out.println("Employees: " + employees);
        Student student = new Student();
        student.setAddress(new Address("str", "city", "zc"));
        student.setFaculty(new Faculty("PGS"));
        student.setFirstName("nick");
        entityManager.getTransaction().begin();
        entityManager.persist(student);
        System.out.println("emp from dep : " + entityManager.find(Department.class, dep.getId()).getEmployees());
        System.out.println("dep : " + dep.getEmployees().size());
        entityManager.unwrap(Session.class).update(student);
        entityManager.getTransaction().commit();
        entityManager.close();
        session.close();

        session = entityManagerFactory.createEntityManager().unwrap(Session.class);
        System.out.println("end : " + session.find(Department.class, dep.getId()).getEmployees());
        session.close();

        session = entityManagerFactory.createEntityManager().unwrap(Session.class);
        Meeting meeting = new Meeting();
        Meeting meeting1 = new Meeting();
        meeting.getStudents().add(student);
        meeting1.getStudents().add(student);
        meeting.setName("meet1");
        meeting1.setName("meet2");
        session.beginTransaction();
        Student st = session.load(Student.class, student.getId());
        st.getMeetings().addAll(Set.of(meeting1, meeting));
        session.saveOrUpdate(st);
        session.getTransaction().commit();
        session.close();
        session = entityManagerFactory.createEntityManager().unwrap(Session.class);
        System.out.println("end students : " + session.find(Student.class, st.getId()).getMeetings());
        System.out.println("end meets : " + session.find(Meeting.class, meeting.getId()).getStudents());
        session.close();

    }

    private static void startDatabase() throws SQLException {
//        new Server().runTool("-tcp", "-web", "-ifNotExists");
    }
}
