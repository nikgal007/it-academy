package com.company.project;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Entity
//@DynamicInsert(value = false)
//@SelectBeforeUpdate(value = false)
//@Polymorphism(type = PolymorphismType.EXPLICIT)
//@Persister(impl = CustomerPersister.class)
//@OptimisticLocking(type = OptimisticLockType.VERSION)
//@Proxy(lazy = false, proxyClass = Customer.class)
//@BatchSize(size = 1)
//@Check(constraints = "")
//@Immutable
//@Where(clause = "firstName = nick")
//@DiscriminatorValue(value = "p")
//@Subselect("select")
//@RowId(value = "ind")
//@SQLInsert(sql = "")
@NoArgsConstructor
@Data
//@Inheritance(strategy = InheritanceType.JOINED)
public class Person
        extends DateEntity
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    @Embedded
    @AttributeOverrides(
            @AttributeOverride(name = "street", column = @Column(name = "home_street"))
    )
    private Address address;
}
