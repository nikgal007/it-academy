package com.company.project;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "employee_details")
public class EmployeeDetails {
    @Id
    @GenericGenerator(
            name = "one-one",
            strategy = "foreign",
            parameters = @Parameter(name = "property", value = "employee")
    )
    @GeneratedValue(generator = "one-one")
    @Access(AccessType.PROPERTY)
    @Column(name = "employee_id")
    private Long id;
    private String description;
    @OneToOne
    @PrimaryKeyJoinColumn
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @Access(AccessType.PROPERTY)
    private Employee employee;
}
