package com.company.project;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;

@Data
@NoArgsConstructor
@AllArgsConstructor
//@Embeddable
@Access(AccessType.PROPERTY)
public class Address {
    private String street;
    private String city;
    private String zipCode;
}
