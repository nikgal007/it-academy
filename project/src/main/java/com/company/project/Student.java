package com.company.project;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Data
@NoArgsConstructor
@Entity
//@DiscriminatorValue("S")
//@Table(name = "STUDENT")
//@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Student extends Person {
    @Embedded
    private Faculty faculty;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "meeting_id"))
    private Set<Meeting> meetings;
}
