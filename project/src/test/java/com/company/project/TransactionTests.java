package com.company.project;

import net.sf.ehcache.CacheManager;
import org.hibernate.Session;
import org.junit.jupiter.api.Test;

import javax.persistence.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TransactionTests {
    private final EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("br.com.fredericci.pu");
    private final EntityManager entityManager = entityManagerFactory.createEntityManager();

    @Test
    void onException() {
        Employee employee = new Employee("Dennys", "Fredericci");
        TestEntity testEntity = new TestEntity("test");

        Session session = entityManager.unwrap(Session.class);
        try {
            session.beginTransaction();
            Department department = new Department("Dennys");
            employee.setDepartment(department);
            department.getEmployees().add(employee);
            session.save(testEntity);
            session.save(employee);
//            fail();
            session.getTransaction().commit();
            session.clear();
        } catch (Exception e) {
            session.getTransaction().rollback();
        } finally {
//            session.getTransaction().commit();
            session.clear();
//            session.close();
        }

        assertEquals(employee, session.createQuery("from Employee e where e.id= :id", Employee.class)
                .setParameter("id", employee.getId()).getSingleResult());

        assertEquals(testEntity, session.createQuery("from TestEntity e where e.id= :id", TestEntity.class)
                .setParameter("id", testEntity.getId()).getSingleResult());
    }

    @Test
    void pessimisticLock() throws InterruptedException {
        Employee employee = new Employee("Dennys", "Fredericci");


        entityManager.getTransaction().begin();
        entityManager.persist(employee);
        entityManager.getTransaction().commit();
        entityManager.clear();

        entityManager.getTransaction().begin();
        Employee emp = entityManager.find(Employee.class, employee.getId(), LockModeType.OPTIMISTIC);
        emp.setFirstName("upd1");
        new Thread(() ->
        {
            EntityManager entityManager1 = entityManagerFactory.createEntityManager();
            entityManager1.getTransaction().begin();
            Employee emp1 = entityManager1.find(Employee.class, employee.getId());
            emp1.setFirstName("upd2");
            entityManager1.getTransaction().commit();
        }).start();
        Thread.sleep(500);
        entityManager.getTransaction().commit();

        entityManager.createQuery("from Employee e where e.id= :id", Employee.class)
                .setParameter("id", employee.getId()).getResultList().forEach(System.out::println);

    }

    @Test
    void transIso() {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
//        IntStream.range(0,10).mapToObj(Integer::toString).map(Department::new).forEach(entityManager::persist);
        try {
            entityManager.createNativeQuery(
                    "INSERT INTO Department VALUES (201, 'cherry1');"
            ).executeUpdate();
            EntityManager em = entityManagerFactory.createEntityManager();
            Department department = em.unwrap(Session.class)
                    .createNativeQuery("select * from Department where department_id=201", Department.class)
                    .setReadOnly(true)
                    .getSingleResult();
            System.out.println(department);
            entityManager.createQuery(
                    "from Department", Department.class).getResultList().forEach(System.out::println);

        } finally {
            transaction.rollback();
        }
    }

    @Test
    public void loadTestCache() {
        System.out.println("size : " + CacheManager.ALL_CACHE_MANAGERS.get(0)
                .getCache("com.company.project.Department").getSize());
        loadDeps();
        loadDeps();
        loadDeps();
        System.out.println("size : " + CacheManager.ALL_CACHE_MANAGERS.get(0)
                .getCache("com.company.project.Department").getSize());
    }

    void loadDeps() {
        Session session = entityManagerFactory.createEntityManager().unwrap(Session.class);
        session.beginTransaction();
        System.out.println(session.get(Department.class, 11L));
//        session.createQuery("from Department where id = 11", Department.class)
//                .setCacheable(true)
//                .list().stream()
//                .findFirst().ifPresent(System.out::println);
        session.getTransaction().commit();
    }

    void fail() {
        throw new RuntimeException();
    }


}