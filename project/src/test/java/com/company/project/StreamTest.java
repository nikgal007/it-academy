package com.company.project;

import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamTest {
    @Test
    void flatMap() {
        Department department = new Department("dName");
        department.setEmployees(IntStream.range(0, 10)
                .mapToObj(id -> new Employee("name" + id, "lastName" + id))
                .peek(employee -> employee.setDepartment(department))
                .peek(System.out::println)
                .collect(Collectors.toSet()));
        Department anotherDepartment = new Department("d1Name");
        anotherDepartment.setEmployees(department.getEmployees());
        System.out.println(department.getEmployees());
        Stream.of(department, anotherDepartment)
                .map(Department::getEmployees)
                .flatMap(Collection::stream)
                .forEach(System.out::println);
    }

    @Test
    void reduce() {
        Department department = new Department("dName");
        department.setEmployees(IntStream.range(0, 10)
                .mapToObj(id -> new Employee("name" + id, "lastName" + id))
                .peek(employee -> employee.setDepartment(department))
                .peek(System.out::println)
                .collect(Collectors.toSet()));

        System.out.println(department.getEmployees().stream().map(Employee::getFirstName).reduce((s, s2) -> s + s2));
    }

    @Test
    void onClose() {
        Department department = new Department("dName");

        Stream<Employee> stream = IntStream.range(0, 10)
                .mapToObj(id -> new Employee("name" + id, "lastName" + id))
                .peek(employee -> employee.setDepartment(department))
                .peek(System.out::println)
                .onClose(new Thread(() -> System.out.println("finished")));
        stream.forEach(System.out::println);
        stream.close();
    }

    @Test
    void orElse() {
        Object obj = null;
        System.out.println(Optional.ofNullable(obj).orElse(foo()));
        System.out.println(Optional.ofNullable(obj).orElseGet(this::foo));

        obj = new Object();
        System.out.println(Optional.ofNullable(obj).orElse(foo()));
        System.out.println(Optional.ofNullable(obj).orElseGet(this::foo));


//        System.out.println(Optional.of(null).orElseGet(this::foo));
    }

    private String foo() {
        System.out.println("foo call");
        return "foo";
    }
}
