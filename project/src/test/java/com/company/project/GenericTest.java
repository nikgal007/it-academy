package com.company.project;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class GenericTest {

    // Source
    List<Integer> intList = Arrays.asList(1, 2, 3);
    List<Double> doubleList = Arrays.asList(2.78, 3.14);
    List<Number> numList = Arrays.asList(1, 2, 2.78, 3.14, 5);

    // Destination
    List<Integer> intList2 = new ArrayList<>();
    List<Double> doublesList2 = new ArrayList<>();
    List<Number> numList2 = new ArrayList<>();


    static <T> void copyElements1(Collection<T> src, Collection<T> dest) {
        dest.addAll(src);
    }



    // copy Integer (? extends T) to its supertype (Number is super of Integer)
    private static <T> void copyElements2(Collection<? extends T> src,
                                          Collection<? super T> dest) {
        dest.addAll(src);
    }

    @Test
    public void genericTest() {
        // Works
        copyElements1(intList, intList2);         // from int to int

        copyElements1(doubleList, doublesList2);  // from double to double

        // Let's try to copy intList to its supertype
//        copyElements1(intList, numList2); // error, method signature just says "T"
        // and here the compiler is given
        // two types: Integer and Number,
        // so which one shall it be?

        // PECS to the rescue!
        copyElements2(intList, numList2);  // possible
    }
}
