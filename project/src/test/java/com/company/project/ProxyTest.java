package com.company.project;

import lombok.Data;
import org.junit.jupiter.api.Test;

import javax.persistence.Entity;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProxyTest {
    @Test
    void proxy() {
        IFoo foo = new Foo();
        IFoo proxyInstance = (IFoo) Proxy.newProxyInstance(
                Foo.class.getClassLoader(),
                new Class[]{IFoo.class},
                new DynamicInvocationHandler(foo));

        proxyInstance.setName("name");
        assertEquals("name", proxyInstance.getName());
        assertEquals("name", foo.getName());
    }


    private class DynamicInvocationHandler implements InvocationHandler {
        private final Map<String, Method> methods = new HashMap<>();

        private Object target;

        public DynamicInvocationHandler(Object target) {
            this.target = target;

            for (Method method : target.getClass().getDeclaredMethods()) {
                this.methods.put(method.getName(), method);
            }
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args)
                throws Throwable {
            System.out.println("annotated : " + target.getClass().isAnnotationPresent(Entity.class));
            System.out.println("Invoked method: " + method.getName());
            Optional.ofNullable(args).stream().flatMap(Arrays::stream).filter(Objects::nonNull).forEach(System.out::println);
            return method.invoke(target, args);
//            return "name";
        }
    }

    @Data
    @Entity
    public class Foo implements IFoo {
        private String name;

    }

    public interface IFoo {
        String getName();
        void setName(String name);
    }
}
