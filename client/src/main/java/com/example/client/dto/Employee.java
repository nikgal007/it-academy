package com.example.client.dto;

import lombok.Data;

@Data
public class Employee {
    private Integer id;
    private String name;
    private Integer age;
}