package com.example.client.dto;

import lombok.Data;

@Data
public class Post {
    private String id;
    private String userId;
    private String title;
    private String body;
}
