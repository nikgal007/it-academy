package com.example.client;

import com.example.client.dto.Employee;
import com.example.client.dto.Post;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(value = "employeeClient", url = "http://localhost:8080/")
public interface EmployeeClient {
    @RequestMapping(method = RequestMethod.GET, value = "/employees")
    List<Employee> getEmployees();

    @RequestMapping(method = RequestMethod.POST, value = "/employees")
    void createEmployee(Employee employee);

    @RequestMapping(method = RequestMethod.GET, value = "/employees/{id}", produces = "application/json")
    Employee getEmployeeById(@PathVariable("id") Long id);
}
